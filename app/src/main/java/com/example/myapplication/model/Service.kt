package com.example.myapplication.model

import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.*

interface Service {

    companion object {
        val baseURL : String = "https://data.cityofnewyork.us/resource/"
    }
//    https://data.cityofnewyork.us/resource/s3k6-pzi2.json

    @GET("s3k6-pzi2.json")
    fun getSchools() : Observable<List<School>>

    //https://data.cityofnewyork.us/resource/s3k6-pzi2.json?dbn=21K344

    @GET("s3k6-pzi2.json")
    fun getSATScores(@Query("dbn") dbn: String): Observable<Score>
}