package com.example.myapplication.model

import io.reactivex.Observable

class SchoolServiceManager {
    fun getSchools() : Observable<List<School>> {
        return RetrofitSetup.getInstance().service().getSchools()
    }

    fun getSchoolScores(dbn: String) : Observable<Score> {
        return RetrofitSetup.getInstance().service().getSATScores(dbn)
    }
}