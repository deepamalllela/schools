package com.example.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class School {

    @SerializedName("dbn")
    private String dbn;
    @SerializedName("school_name")
    private String school_name;
    @SerializedName("overview_paragraph")
    private String overview_paragraph;

    public String getDbn() {
        return dbn;
    }

    public String getSchool_name() {
        return school_name;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }
}
