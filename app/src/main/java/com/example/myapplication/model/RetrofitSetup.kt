package com.example.myapplication.model


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitSetup() {

     var service: Service

    init {
        val retroft = Retrofit.Builder().baseUrl(Service.baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        service = retroft.create(Service::class.java)
    }

    companion object {
        lateinit var retrofitSetup: RetrofitSetup

        @JvmStatic
        fun getInstance(): RetrofitSetup {
            if (this::retrofitSetup.isInitialized) return retrofitSetup
            else
                retrofitSetup = RetrofitSetup()
            return retrofitSetup
        }
    }

    fun service(): Service {
        return service
    }
}