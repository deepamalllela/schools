package com.example.myapplication.viewmodels;

import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.SchoolServiceManager;
import com.example.myapplication.model.Score;

import io.reactivex.Observable;

public class DetailsViewModel extends ViewModel {

    SchoolServiceManager serviceManager = new SchoolServiceManager();

    public Observable<Score> getScores(String dbn) {
        // Sending a placeholder value when API doesn't return any test score
        Score score = new Score();
        score.setSat_writing_avg_score("100");
        score.setSat_critical_reading_avg_score("50");
        score.setSat_math_avg_score("100");
        return serviceManager.getSchoolScores(dbn)
                .onErrorReturnItem(score);
    }
}
