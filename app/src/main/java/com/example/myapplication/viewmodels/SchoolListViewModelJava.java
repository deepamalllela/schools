package com.example.myapplication.viewmodels;

import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.School;
import com.example.myapplication.model.SchoolServiceManager;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class SchoolListViewModelJava extends ViewModel {

    public PublishSubject<List<School>> publishSchools = PublishSubject.create();

    SchoolServiceManager schoolServiceManager; // Time permitted will use a Dependency Injection

    public SchoolListViewModelJava() {
        schoolServiceManager = new SchoolServiceManager();
    }

    public Observable<List<School>> getSchools() {
        return  schoolServiceManager.getSchools();
    }
}
