package com.example.myapplication.views;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.School;

import java.util.Collections;
import java.util.List;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolViewHolder> {
    List<School> list = Collections.emptyList();
    Context context;
    ClickListiner listiner;

    public  SchoolAdapter(List<School> schools, Context context, ClickListiner listiner) {
        this.list = schools;
        this.context = context;
        this.listiner = listiner;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View schoolView = inflater.inflate(R.layout.layout_school_detail, parent, false);
        SchoolViewHolder viewHolder = new SchoolViewHolder(schoolView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        final int index = holder.getAdapterPosition();
        holder.schoolName.setText(list.get(index).getSchool_name());

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                listiner.click(index, list.get(index).getDbn());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
