package com.example.myapplication.views;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.model.Score;
import com.example.myapplication.viewmodels.DetailsViewModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DetailsActivity extends AppCompatActivity {

    DetailsViewModel viewModel;
    TextView mathScore;
    TextView readingScore;
    TextView writingScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        String dbn = getIntent().getExtras().getString("dbn");
        viewModel = new DetailsViewModel();

        mathScore = findViewById(R.id.math_score);
        readingScore = findViewById(R.id.reading_score);
        writingScore = findViewById(R.id.writing_score);

        getSchools(dbn);
    }

    private void getSchools(String dbn) {
        viewModel.getScores(dbn)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(score -> {
                            if (score.getDbn().isEmpty() && score.getSat_critical_reading_avg_score().isEmpty()) {
                                score = new Score();
                                score.setSat_critical_reading_avg_score("100");
                                score.setSat_math_avg_score("50");
                                score.setSat_writing_avg_score("130");
                            }


                            mathScore.setText(score.getSat_math_avg_score());
                            readingScore.setText(score.getSat_critical_reading_avg_score());
                            writingScore.setText(score.getSat_writing_avg_score());
                        },
                        throwable -> {
                            mathScore.setText("100");
                            readingScore.setText("50");
                            writingScore.setText("130");
                        });
    }

}
