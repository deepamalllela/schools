package com.example.myapplication.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.School;
import com.example.myapplication.model.SchoolServiceManager;
import com.example.myapplication.viewmodels.SchoolListViewModelJava;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SchoolAdapter adapter;
    SchoolListViewModelJava schoolListViewModelJava;
    ClickListiner listiner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        schoolListViewModelJava = new SchoolListViewModelJava();

        listiner = new ClickListiner() {
            @Override
            public void click(int index, String dbn) {
                Bundle bundle = new Bundle();
                bundle.putString("dbn", dbn);
                startActivity(new Intent(MainActivity.this, DetailsActivity.class).putExtras(bundle));
            }
        };

        getSchools();
    }

    private void getSchools() {
        schoolListViewModelJava.getSchools()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    adapter = new SchoolAdapter(list, getApplicationContext(), listiner);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                }, Throwable::printStackTrace);
    }

}



