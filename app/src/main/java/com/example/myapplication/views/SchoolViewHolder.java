package com.example.myapplication.views;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;

public class SchoolViewHolder extends RecyclerView.ViewHolder {
    private final Context context;
    TextView schoolName;
    Button  button;
    View view;

    public SchoolViewHolder(@NonNull View itemView) {
        super(itemView);
        schoolName = (TextView)itemView.findViewById(R.id.school_name);
        button = (Button) itemView.findViewById(R.id.view_detail);
        view = itemView;
        context= itemView.getContext();
    }
}
